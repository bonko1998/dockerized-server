from os.path import abspath
from os import getenv


class Config(object):
    DEBUG = True if not getenv("DEBUG") else getenv("DEBUG")
    TESTING = False if not getenv("TESTING") else getenv("TESTING")
    DEVELOPMENT = True if not getenv("DEVELOPMENT") else getenv("DEVELOPMENT")
    CSRF_ENABLED = False if not getenv("CSRF_ENABLED") else getenv("CSRF_ENABLED")
    SECRET_KEY = (
        "secret-key" if not getenv("FLASK_SECRET_KEY") else getenv("FLASK_SECRET_KEY")
    )
    SQLALCHEMY_DATABASE_URI = (
        f"postgresql://{getenv('POSTGRES_USER')}:{getenv('POSTGRES_PASSWORD')}"
        f"@{getenv('POSTGRES_HOST')}:{getenv('POSTGRES_PORT')}/{getenv('POSTGRES_DB')}"
    )


class DevelopmentConfig(Config):
    pass


class TestingConfig(Config):
    TESTING = True
    DEBUG = False


class StagingConfig(Config):
    DEBUG = True
    DEVELOPMENT = False
    CSRF_ENABLED = True


class ProductionConfig(Config):
    DEBUG = False
    DEVELOPMENT = False
    CSRF_ENABLED = True
